import socket
import sys
import time

with open(sys.argv[1], "rb") as f:
    result = f.read()

s = socket.socket()

for _ in range(5):
    try:
        s.connect(('192.168.56.102', 4321))
        break
    except:
        time.sleep(2)

s.send(result)
s.recv(1024)
s.close()
